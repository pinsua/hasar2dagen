﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Hasar2daGen
{
    class Clientes
    {
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string documento { get; set; }
        public string tipoDocumento { get; set; }
        public string responsabilidIVA { get; set; }

        public static Clientes GetCliente(int _codCliente, SqlConnection _con)
        {
            string _sql = "SELECT CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.NIF20, CLIENTES.CODPOSTAL, " +
                "CLIENTES.POBLACION, CLIENTES.PROVINCIA FROM CLIENTES WHERE CODCLIENTE = @CodCliente";

            Clientes _cls = new Clientes();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@CodCliente", _codCliente);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cls.nombre = _reader["NOMBRECLIENTE"].ToString();
                            _cls.direccion = _reader["DIRECCION1"].ToString() + ", " + _reader["POBLACION"].ToString() + ", " + _reader["PROVINCIA"].ToString();
                            _cls.documento = _reader["NIF20"].ToString();
                        }
                    }
                }
            }

            return _cls;
        }
    }
}
