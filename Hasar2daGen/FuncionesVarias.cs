﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Hasar2daGen
{
    public class FuncionesVarias
    {
        /// <summary>
        /// Calcula el dígito verificador dado un CUIT completo o sin él.
        /// </summary>
        /// <param name="cuit">El CUIT como String sin guiones</param>
        /// <returns>El valor del dígito verificador calculado.</returns>
        public static int CalcularDigitoCuit(string cuit)
        {
            int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            char[] nums = cuit.ToCharArray();
            int total = 0;
            for (int i = 0; i < mult.Length; i++)
            {
                total += int.Parse(nums[i].ToString()) * mult[i];
            }
            int resto = total % 11;
            return resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
        }

        public static bool FechaOk(hfl.argentina.HasarImpresoraFiscalRG3561 _cf, Cabecera _cab)
        {
            bool _rta = false;
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora _respFechaHora = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora();
            _respFechaHora = _cf.ConsultarFechaHora();

            DateTime _fecha = _respFechaHora.getFecha();

            if (_cab.fecha.Date == _fecha.Date)
                _rta = true;
            else
            {
                if (_cab.fecha.Date >= _fecha.Date.AddDays(-3) && _cab.fecha.Date < _fecha.Date)
                    _rta = true;
            }

            return _rta;
        }

        public static bool TengoComprobantesSinImprimmir(SqlConnection _conn, string _caja)
        {
            bool _rta = true;

            string _sql = "SELECT count(*) FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on " +
                "FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC " +
                "AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE(FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') " +
                "AND ALBVENTACAB.N = 'B' AND ALBVENTACAB.CAJA = @caja AND ALBVENTACAB.FECHA = @date";

            using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
            {
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                object _resp = _cmd.ExecuteScalar();

                if (_resp == null)
                    _rta = false;
                else
                {
                    if (Convert.ToInt32(_resp) > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }

            return _rta;
        }

        public static bool FaltaPapel(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            if (_cf.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
            {
                return true;
            }
            else
                return false;
        }
    }
}
