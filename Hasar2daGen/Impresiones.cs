﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class Impresiones
    {
        public static string ImprimirNotaCreditoAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _oTributos,
                List<Recargos> _recargos, List<DocumentoAsociado> _docAsoc, string _ip, SqlConnection _sqlConn, out bool _faltaPapel)
        {
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Abrimos el log.
            hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    //Sacado.
                    //hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO);

                    if (_cab.regfacturacioncliente == 4)
                    {
                        hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cab.direccioncliente, "", "", "");

                        //Documentos Asociados.
                        int _counter = 1;
                        foreach(DocumentoAsociado _doc in _docAsoc)
                        {
                            switch(_doc._seriefiscal2.Substring(0, 3).ToUpper())
                            {
                                case "001":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal));
                                        break;
                                    }
                                case "006":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal));
                                        break;
                                    }
                                default:
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal));
                                    break;
                            }
                            _counter++;
                        }
                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                    }
                    else
                    {
                        hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                            _cab.direccioncliente, "", "", "");

                        //Documentos Asociados.
                        int _counter = 1;
                        foreach (DocumentoAsociado _doc in _docAsoc)
                        {
                            switch (_doc._seriefiscal2.Substring(0, 3).ToUpper())
                            {
                                case "001":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal));
                                        break;
                                    }
                                case "006":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal));
                                        break;
                                    }
                                default:
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal));
                                    break;
                            }
                            _counter++;
                        }

                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                    }

                    foreach (Items _it in _items)
                    {
                        if(!String.IsNullOrEmpty(_it._referencia))
                            hasar.ImprimirTextoFiscal(_estilo, "Referencia " + _it._referencia, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);

                        if (String.IsNullOrEmpty(_it._descripcion))
                            _it._descripcion = "Articulo sin Descripcion";

                        hasar.ImprimirItem(_it._descripcion,
                            Math.Abs(Convert.ToDouble(_it._cantidad)),
                            Math.Abs(Convert.ToDouble(_it._precio)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            0.0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        //Veo si tengo dto en el item.
                        if (_it._descuento > 0)
                        {
                            double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad *_it._precio) * _it._descuento / 100)));
                            //Valido que tengo texto descuento.
                            if (String.IsNullOrEmpty(_it._textoPromo))
                            {
                                if(_it._descuento == 0)
                                    _it._textoPromo = "Descuento ";
                                else
                                    _it._textoPromo = "Descuento " + _it._descuento.ToString();
                            }
                            hasar.ImprimirDescuentoItem(_it._textoPromo,
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    if (_cab.dtocomercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
                            Math.Abs(_cab.totdtocomercial),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_cab.dtopp != 0)
                    {
                        if (_cab.dtopp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _cab.dtopp.ToString(),
                                Math.Abs(_cab.totdtopp),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_cab.totdtopp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _cab.dtopp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }

                    //Recargos
                    foreach (Recargos _rec in _recargos)
                    {
                        double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                        hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.COBRO_ANTICIPO);

                    }

                    //Otros Tributos
                    foreach (OtrosTributos _ot in _oTributos)
                    {
                        double _impor = Math.Abs(_ot._importe);
                        hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB,
                            _ot._nombre, _ot._base, _impor);
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.tipopago.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    break;
                                    _abrirCajon = true;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                    }

                    //_subTotal = hasar.ConsultarSubtotal();

                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();

                    string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');

                    bool _ok = FacturasVentaSerieResol.GrabarTiquet(_cab.numserie, _cab.numfac, _cab.n, _numeroPos, _numeroPos, _numeroTicket, _sqlConn);

                    if (!_ok)
                    {
                        string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                }
                catch (Exception ex)
                {
                    Impresiones.Cancelar(hasar);
                    throw new Exception(ex.Message);
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        public static string ImprimirFacturasAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _tributos,
            List<Recargos> _recargos, string _ip, SqlConnection _sqlConn, out bool _faltaPapel)
        {
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            
            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                //hasar.conectar("127.0.0.1", 6000);
                hasar.conectar(_ip);

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {

                    if (_cab.regfacturacioncliente == 4)
                    {
                        hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cab.direccioncliente, "", "", "");

                        _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_B);
                    }
                    else
                    {
                        hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                            _cab.direccioncliente, "", "", "");

                        _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                    }

                    foreach (Items _it in _items)
                    {
                        if (!String.IsNullOrEmpty(_it._referencia))
                            hasar.ImprimirTextoFiscal(_estilo, "Referencia " + _it._referencia, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);

                        if (String.IsNullOrEmpty(_it._descripcion))
                            _it._descripcion = "Articulo Sin Descripcion";


                        hasar.ImprimirItem(_it._descripcion,
                            Convert.ToDouble(_it._cantidad),
                            Convert.ToDouble(_it._precio),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            0.0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        //Veo si tengo dto en el item.
                        if (_it._descuento > 0)
                        {
                            double _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100));
                            //Valido que tengo texto descuento.
                            if (String.IsNullOrEmpty(_it._textoPromo))
                            {
                                if (_it._descuento == 0)
                                    _it._textoPromo = "Descuento ";
                                else
                                    _it._textoPromo = "Descuento " + _it._descuento.ToString();
                            }
                            hasar.ImprimirDescuentoItem(_it._textoPromo,
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    if (_cab.dtocomercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
                            _cab.totdtocomercial,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_cab.dtopp != 0)
                    {
                        if (_cab.dtopp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _cab.dtopp.ToString(),
                                _cab.totdtopp,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_cab.totdtopp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _cab.dtopp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }

                    //Recargos
                    foreach (Recargos _rec in _recargos)
                    {
                        double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                        hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.COBRO_ANTICIPO);

                    }

                    //Otros Tributos.
                    foreach (OtrosTributos _ot in _tributos)
                    {
                        double _impor = Math.Abs(_ot._importe);
                        hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB,
                            _ot._nombre, _ot._base, _impor);
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.tipopago.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_cab.entregado == 0)
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    else
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_cab.entregado),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    _abrirCajon = true;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");

                                    break;
                                }
                        }
                    }

                    _respCerrarDoc = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();

                    string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');

                    bool _Ok = FacturasVentaSerieResol.GrabarTiquet(_cab.numserie, _cab.numfac, _cab.n, _numeroPos, _cab.descripcionticket, _numeroTicket, _sqlConn);

                    if (!_Ok)
                    {
                        string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;                        
                    }

                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                }
                else
                {
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
                }

            }
            catch (Exception ex)
            {
                string _error = "La venta quedará registrada PENDIENTE de fiscalizar. Revise el error y REINTENTE IMPRIMIR el ticket.";
                Impresiones.Cancelar(hasar);
                throw new Exception(ex.Message);                
            }

            return _rta;
        }

        public static void ImprimirTicketRegalo(Cabecera _cab, List<Items> _items, string _ip
            , string _txtRegalo1, string _txtRegalo2, string _txtRegalo3,
            string _comprobante, out bool _faltaPapel)
        {            
            string _rta = "";
            _faltaPapel = false;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);

            try
            {
                //Abrimos el log.
                hasar.archivoRegistro("HasarLog.log"); //todo
                //Conectamos con la hasar.
                hasar.conectar(_ip);

                //Abro el documento.
                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);

                _estilo.setNegrita(true);
                //Titulo.
                _estilo.setDobleAncho(true);
                _estilo.setNegrita(true);
                _estilo.setCentrado(true);
                hasar.ImprimirTextoGenerico(_estilo, "Ticket Regalo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                hasar.ImprimirTextoGenerico(_estilo, "Comprobante: " +_comprobante, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setCentrado(false);
                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);

                //Cabecera de Items.
                hasar.ImprimirTextoGenerico(_estilo, "Descripcion Articulo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //Linea separadora.
                hasar.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Recorro los items.
                decimal _cantidad = 0;
                foreach (Items _it in _items)
                {
                    //Imprimo items
                    hasar.ImprimirTextoGenerico(_estilo, _it._descripcion, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                    _cantidad = _cantidad + _it._cantidad;
                }
                _estilo.setNegrita(true);
                //Linea separadora.
                hasar.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                hasar.ImprimirTextoGenerico(_estilo, "Total unidades: " + Convert.ToInt32(_cantidad).ToString(), hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Imprimo texto de regalo.
                //_estilo.setDobleAncho(true);
                if(!String.IsNullOrEmpty(_txtRegalo1))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo2))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo2, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo3))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo3, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //_estilo.setDobleAncho(false);

                _respCerrarDoc = hasar.CerrarDocumento();

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;
                    //_rta = "La impresora fiscal informa que se esta quedando sin papel. Por favor revise el papel.";
            }
            catch(Exception ex)
            {
                Impresiones.Cancelar(hasar);
                throw new Exception(ex.Message);
            }
        }

        public static string Reimprimir(int _nro, string _descrip, string _ip, out bool _faltaPapel)
        {
            string _rta = "";
            _faltaPapel = false;
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();

            hasar.conectar(_ip);

            try
            {
                switch (_descrip.Substring(0, 3))
                {
                    //case "NOTA DE CREDITO A":
                    case "003":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A, _nro);
                            break;
                        }
                    //case "NOTA DE CREDITO B":
                    case "008":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B, _nro);
                            break;
                        }
                    //case "TIQUET FACTURA A":
                    case "001":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, _nro);
                            break;
                        }
                    //case "TIQUET FACTURA B":
                    case "006":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, _nro);
                            break;
                        }
                    case "002":
                        {
                            //Nota de debito A
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_A, _nro);
                            break;
                        }
                    case "007":
                        {
                            //Nota de debito A
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_B, _nro);
                            break;
                        }
                    default:
                        {
                            _rta = "Tipo de Documento no configurado para su fiscalización.";
                            break;
                        }
                }

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;
                    //_rta = "La impresora fiscal informa que se esta quedando sin papel. Por favor revise el papel.";
            }
            catch (Exception ex)
            {
                Cancelar(hasar);
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static void Cancelar(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal _estadoFiscal = new hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _respConsultarEstado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
 

            _respConsultarEstado = _cf.ConsultarEstado();
            _estadoFiscal = _cf.ObtenerUltimoEstadoFiscal();

            if (_estadoFiscal.getDocumentoAbierto())
            {
 
                try
                {
                    _cf.Cancelar();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
            }
        }

    }
}
