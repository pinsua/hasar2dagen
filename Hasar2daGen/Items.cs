﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Hasar2daGen
{
    public class Items
    {
        public string _codArticulo { get; set; }
        public string _descripcion { get; set; }
        public decimal _cantidad { get; set; }
        public decimal _precio { get; set; }
        public decimal _iva { get; set; }
        public decimal _descuento { get; set; }
        public string _textoPromo { get; set; }
        public string _abonoDeNumseie { get; set; }
        public int _abonoDeNumAlbaran { get; set; }
        public string _abonoDe_N { get; set; }
        public string _referencia { get; set; }

        public static List<Items> GetItems(string _numSerie, string _n, int _nroAlbaran, SqlConnection _con)
        {
            string _sql = "SELECT ALBVENTALIN.CODARTICULO, ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL, ALBVENTALIN.PRECIO, " +
                "ALBVENTALIN.IVA, ALBVENTALIN.DTO, PROMOCIONES.TEXTOIMPRIMIR, ALBVENTALIN.ABONODE_NUMSERIE, ALBVENTALIN.ABONODE_NUMALBARAN, " +
                "ALBVENTALIN.ABONODE_N, ALBVENTALIN.REFERENCIA " +
                "FROM ALBVENTALIN left join ALBVENTALINPROMOCIONES ON ALBVENTALIN.NUMSERIE = ALBVENTALINPROMOCIONES.NUMSERIE " +
                "AND ALBVENTALIN.NUMALBARAN = ALBVENTALINPROMOCIONES.NUMALBARAN " +
                "AND ALBVENTALIN.N = ALBVENTALINPROMOCIONES.N " +
                "AND ALBVENTALIN.NUMLIN = ALBVENTALINPROMOCIONES.NUMLIN " +
                "LEFT JOIN PROMOCIONES ON ALBVENTALINPROMOCIONES.IDPROMOCION = PROMOCIONES.IDPROMOCION " +
                "WHERE ALBVENTALIN.NUMSERIE = @Numserie and ALBVENTALIN.N = @N and ALBVENTALIN.NUMALBARAN = @NumAlbaran " +
                "ORDER BY ALBVENTALIN.UNIDADESTOTAL Desc";
            

            List<Items> _items = new List<Items>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@NumAlbaran", _nroAlbaran);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Items _cls = new Items();
                            _cls._cantidad = Convert.ToDecimal(_reader["UNIDADESTOTAL"]);
                            _cls._codArticulo = _reader["CODARTICULO"].ToString();
                            _cls._descripcion = _reader["DESCRIPCION"].ToString();
                            _cls._descuento = Convert.ToDecimal(_reader["DTO"]);
                            _cls._iva = Convert.ToDecimal(_reader["IVA"]);
                            _cls._precio = Convert.ToDecimal(_reader["PRECIO"]);
                            _cls._textoPromo = _reader["TEXTOIMPRIMIR"].ToString();
                            _cls._abonoDeNumseie = _reader["ABONODE_NUMSERIE"].ToString();
                            _cls._abonoDeNumAlbaran = Convert.ToInt32(_reader["ABONODE_NUMALBARAN"]);
                            _cls._abonoDe_N = _reader["ABONODE_N"].ToString();
                            _cls._referencia = _reader["REFERENCIA"].ToString();

                            _items.Add(_cls);
                        }
                    }
                }
            }

            return _items;
        }
    }
}
